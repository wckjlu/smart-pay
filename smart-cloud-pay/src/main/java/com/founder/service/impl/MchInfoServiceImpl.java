package com.founder.service.impl;

import com.founder.core.dao.MchInfoRespository;
import com.founder.core.domain.MchInfo;
import com.founder.service.IMchInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MchInfoServiceImpl implements IMchInfoService {

    @Autowired
    MchInfoRespository mchInfoRespository;

    @Override
    public MchInfo selectMchInfo(String mchId) {
        Optional<MchInfo> optional = mchInfoRespository.findById(mchId);
        MchInfo mchInfo = null;
        if (optional.isPresent()){
            mchInfo = optional.get();
        }
        return mchInfo;
    }

}
