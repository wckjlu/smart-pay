package com.founder.config;

import com.founder.core.log.MyLog;
import com.xxl.job.core.executor.XxlJobExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.founder.jobhandler")
public class XxlJobConfig {

    private final static MyLog logger = MyLog.getLog(XxlJobConfig.class);

    @Value("${xxl.job.admin.addresses}")
    private String addresses;

    @Value("${xxl.job.executor.appname}")
    private String appname;

    @Value("${xxl.job.executor.ip}")
    private String ip;

    @Value("${xxl.job.executor.port}")
    private int port;

    @Value("${xxl.job.executor.logpath}")
    private String logpath;

    @Value("${xxl.job.accessToken}")
    private String accessToken;

    @Value("${xxl.job.executor.logretentiondays}")
    private int logRetentionDays;

    @Value("${config.web.base_url}")
    private String base_url;

    @Value("${config.web.req_key}")
    private String req_key;

    @Value("${config.web.bill_path}")
    private String bill_path;

    @Value("${config.web.rep_key}")
    private String rep_key;

    @Value("${config.ftp.ftpOpen}")
    private String ftp_open;

    @Value("${config.ftp.ftpIP}")
    private String ftp_iP;

    @Value("${config.ftp.ftpPort}")
    private String ftp_port;

    @Value("${config.ftp.ftpUser}")
    private String ftp_user;

    @Value("${config.ftp.ftpPass}")
    private String ftp_pass;

    @Value("${config.hostipal.code}")
    private String hostipal_code;

    @Value("${config.regex.mz}")
    private String regex_mz;

    @Value("${config.regex.zy}")
    private String regex_zy;

    @Value("${config.regex.app}")
    private String regex_app;

    @Value("${config.cron.download-wx}")
    private String download_wx;

    @Value("${config.cron.download-wx-check}")
    private String download_wx_check;

    @Value("${config.cron.download-ali}")
    private String download_ali;

    @Value("${config.cron.download-ali-check}")
    private String download_ali_check;

    public String getBase_url() {
        return base_url;
    }

    public void setBase_url(String base_url) {
        this.base_url = base_url;
    }

    public String getReq_key() {
        return req_key;
    }

    public void setReq_key(String req_key) {
        this.req_key = req_key;
    }

    public String getRep_key() {
        return rep_key;
    }

    public void setRep_key(String rep_key) {
        this.rep_key = rep_key;
    }

    public String getBill_path() {
        return bill_path;
    }

    public void setBill_path(String bill_path) {
        this.bill_path = bill_path;
    }

    public String getFtp_open() {
        return ftp_open;
    }

    public void setFtp_open(String ftp_open) {
        this.ftp_open = ftp_open;
    }

    public String getFtp_iP() {
        return ftp_iP;
    }

    public void setFtp_iP(String ftp_iP) {
        this.ftp_iP = ftp_iP;
    }

    public String getFtp_port() {
        return ftp_port;
    }

    public void setFtp_port(String ftp_port) {
        this.ftp_port = ftp_port;
    }

    public String getFtp_user() {
        return ftp_user;
    }

    public void setFtp_user(String ftp_user) {
        this.ftp_user = ftp_user;
    }

    public String getFtp_pass() {
        return ftp_pass;
    }

    public void setFtp_pass(String ftp_pass) {
        this.ftp_pass = ftp_pass;
    }

    public String getHostipal_code() {
        return hostipal_code;
    }

    public void setHostipal_code(String hostipal_code) {
        this.hostipal_code = hostipal_code;
    }

    public String getRegex_mz() {
        return regex_mz;
    }

    public void setRegex_mz(String regex_mz) {
        this.regex_mz = regex_mz;
    }

    public String getRegex_zy() {
        return regex_zy;
    }

    public void setRegex_zy(String regex_zy) {
        this.regex_zy = regex_zy;
    }

    public String getRegex_app() {
        return regex_app;
    }

    public void setRegex_app(String regex_app) {
        this.regex_app = regex_app;
    }

    public String getDownload_wx() {
        return download_wx;
    }

    public void setDownload_wx(String download_wx) {
        this.download_wx = download_wx;
    }

    public String getDownload_wx_check() {
        return download_wx_check;
    }

    public void setDownload_wx_check(String download_wx_check) {
        this.download_wx_check = download_wx_check;
    }

    public String getDownload_ali() {
        return download_ali;
    }

    public void setDownload_ali(String download_ali) {
        this.download_ali = download_ali;
    }

    public String getDownload_ali_check() {
        return download_ali_check;
    }

    public void setDownload_ali_check(String download_ali_check) {
        this.download_ali_check = download_ali_check;
    }

    @Bean(initMethod = "start", destroyMethod = "destroy")
    public XxlJobExecutor xxlJobExecutor() {
        logger.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobExecutor xxlJobExecutor = new XxlJobExecutor();
        xxlJobExecutor.setIp(ip);
        xxlJobExecutor.setPort(port);
        xxlJobExecutor.setAppName(appname);
        xxlJobExecutor.setAdminAddresses(addresses);
        xxlJobExecutor.setLogPath(logpath);
        xxlJobExecutor.setAccessToken(accessToken);
        xxlJobExecutor.setLogRetentionDays(logRetentionDays);
        return xxlJobExecutor;
    }
}
