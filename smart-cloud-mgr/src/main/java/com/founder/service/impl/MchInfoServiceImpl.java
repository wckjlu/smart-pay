package com.founder.service.impl;

import com.founder.core.dao.MchInfoRespository;
import com.founder.core.domain.MchInfo;
import com.founder.core.log.MyLog;
import com.founder.service.IMchInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MchInfoServiceImpl implements IMchInfoService {

    private static final MyLog _log = MyLog.getLog(MchInfoServiceImpl.class);

    @Autowired
    MchInfoRespository mchInfoRespository;

    @Override
    public Integer count(MchInfo mchInfo) {
        Example<MchInfo> example = Example.of(mchInfo);
        Long count = mchInfoRespository.count(example);
        return count.intValue();
    }

    @Override
    public List<MchInfo> getMchInfoList(int offset, int limit, MchInfo mchInfo) {
        Pageable pageable = PageRequest.of(offset,limit);
        Example<MchInfo> example = Example.of(mchInfo);
        Page<MchInfo> page = mchInfoRespository.findAll(example,pageable);
        return page.getContent();
    }

    @Override
    public MchInfo selectMchInfo(String mchId) {
        MchInfo mchInfo = null;
        Optional<MchInfo> optional = mchInfoRespository.findById(mchId);
        if (optional.isPresent()){
            _log.info("查到记录");
            mchInfo = optional.get();
        }
        return mchInfo;
    }

    @Override
    public int addMchInfo(MchInfo mchInfo) {
        Pageable pageable = PageRequest.of(0, 1);
        Page<MchInfo> page = mchInfoRespository.findAll(new Specification<MchInfo>() {
            @Override
            public Predicate toPredicate(Root<MchInfo> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = criteriaBuilder.conjunction();
                if (StringUtils.isNotBlank(mchInfo.getMchId())) {
                    predicate.getExpressions().add(criteriaBuilder.equal(root.get("mchId"), mchInfo.getMchId().trim()));
                }
                criteriaQuery.where(predicate);
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get("mchId").as(String.class)));
                return criteriaQuery.getRestriction();
            }
        }, pageable);

        String mchId = "10000000";
        if (page.getTotalPages() > 0) {
            mchId = String.valueOf(Integer.parseInt(page.getContent().get(0).getMchId()) + 1);
        }

        mchInfo.setMchId(mchId);
        mchInfo.setCreateTime(new Date());
        MchInfo mch = mchInfoRespository.save(mchInfo);
        return mch == null ? 0 : 1;
    }

    @Override
    public int updateMchInfo(MchInfo mchInfo) {
        MchInfo mch = mchInfoRespository.save(mchInfo);
        return mch == null ? 0 : 1;
    }

}
